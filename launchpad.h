#ifndef LAUNCHPAD_H
#define LAUNCHPAD_H

#include <iostream>
#include <string>
#include <cstdint>

//c functions
#include <cstring>
#include <unistd.h>
#include <fcntl.h>

class Event{
public:
    Event(uint8_t key_, bool pressed_): key(key_), is_pressed(pressed_) {}

    uint8_t key;
    bool is_pressed;

    bool key_pressed(uint8_t key_){
        return key==key_&& is_pressed;
    }

    bool key_released(uint8_t key_){
        return key==key_&& !is_pressed;
    }

    bool is_keypad(){
        return ((key%16)<8 && (key/16)<8);
    }


};

class Launchpad{
private:
    int fd;

public:
    Launchpad(std::string dev = "/dev/midi"){
        fd = open(dev.c_str(), O_RDWR, 0);
        if (fd < 0){
            std::cerr << "cannot open "<< dev << ", " << std::strerror(errno) << std::endl;
            exit(1);
        }
    }

    ~Launchpad(){
        close(fd);
    }

    uint8_t LedGetColor( uint8_t red, uint8_t green ){
        red = std::min( (int)red, 3 );
        green = std::min( (int)green, 3 );
        return red | green << 4;
    }

    void LedCtrlRaw(uint8_t button, uint8_t color){
        unsigned char data[3] = {0x90, button, color};
        int r =write(fd, data, sizeof(data));
        if(r<0)
            std::cerr << "write failed: " << std::strerror(errno) << std::endl;
    }

    void LedCtrlRG(uint8_t button, uint8_t red, uint8_t green){
        LedCtrlRaw(button,LedGetColor(red,green));
    }

    Event WaitEvent(){
        unsigned char data[3];
        read(fd, data, 3);
        return Event(data[1], data[2]!=0);
    }



};

/****AN EXAMPLE:

   Launchpad lp("/dev/midi2");

   for(int i=0;i<=3;i++){
        for(int j=0;j<=3;j++){
            lp.LedCtrlRG(i+16*j,i,j);
        }
    }
*/
#endif // LAUNCHPAD_H
