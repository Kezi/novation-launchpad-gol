#ifndef GAMEOFLIFE_H
#define GAMEOFLIFE_H
#include <array>


class GOL{
public:
    static const int size=8;

    std::array<std::array<int, size>, size> cells={0};

    int neighbors(size_t x, size_t y){
        int tot=0;

        size_t xminus1=x-1;
        size_t xplus1=x+1;
        size_t yminus1=y-1;
        size_t yplus1=y+1;

        if(x==0)
            xminus1=size-1;

        if(y==0)
            yminus1=size-1;

        if(x==size-1)
            xplus1=0;

        if(y==size-1)
            yplus1=0;

        tot+=cells[xminus1][y];
        tot+=cells[xminus1][yminus1];
        tot+=cells[xminus1][yplus1];

        tot+=cells[x][yplus1];
        tot+=cells[x][yminus1];

        tot+=cells[xplus1][y];
        tot+=cells[xplus1][yminus1];
        tot+=cells[xplus1][yplus1];

        return tot;

    }

    void think(){
        std::array<std::array<int, size>, size> cells2=cells;

        for(size_t i=0;i<size;i++){
            for(size_t j=0;j<size;j++){
                int n = neighbors(i,j);

                if(cells[i][j]){
                    if( n<2 || n > 3)
                        cells2[i][j]=0;

                }else{
                    if(n==3){
                        cells2[i][j]=1;
                    }
                }

            }
        }

        cells=cells2;
    }



};
#endif // GAMEOFLIFE_H
