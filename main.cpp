
#include "launchpad.h"
#include "gameoflife.h"


#include <iostream>
#include <algorithm>


#include <chrono>
#include <thread>

#include <queue>


int main(void) {
    Launchpad lp("/dev/midi2");
    GOL gol;
    GOL gol_green;

    int run=1;
    int using_red=0;

    lp.LedCtrlRG(120,(!run)*3,(run)*3);
    lp.LedCtrlRG(8,(using_red)*3,(!using_red)*3);


    std::queue<Event> events;

    std::thread lol([&lp,&events](){
        while(1){
            auto lol = lp.WaitEvent();
            events.push(lol);
        }
    });

    while(1){
        for(int i=0;i<gol.size;i++){
            for(int j=0;j<gol.size;j++){
                lp.LedCtrlRG(i+16*j,3*gol.cells[i][j],3*gol_green.cells[i][j]);

            }
        }

        if(run){
            gol.think();
            gol_green.think();
        }

        while(!events.empty()){ //process events from launchpad
            Event lol = events.front();
            events.pop();

            //std::cout << (int)lol.key << std::endl;

            if(lol.is_pressed){
                switch(lol.key){
                case 120:
                    run=!run;
                    lp.LedCtrlRG(120,(!run)*3,(run)*3);
                    break;
                case 8:
                    using_red=!using_red;
                    lp.LedCtrlRG(8,(using_red)*3,(!using_red)*3);
                    break;
                case 104:
                    gol.think();
                    gol_green.think();
                    lp.LedCtrlRG(104,0,3);
                    break;
                }
            }



            if(lol.key_released(104) && !run){
                lp.LedCtrlRG(104,0,0);
                continue;
            }

            if(lol.is_pressed && lol.is_keypad()){ //alla pressione di un tasto
                if(using_red)
                    gol.cells[lol.key%16][lol.key/16]=1;
                else
                    gol_green.cells[lol.key%16][lol.key/16]=1;
            }
        }


        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }



   return 0;
}

